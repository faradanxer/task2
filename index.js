
const { sequelize } = require("..");
const { Sequelize } = require("sequelize");
import { writeFileSync } from "fs";

const sequelize = new Sequelize({
  host: 'database',
  dialect: "postgres",
  database: "db",
  username: 'postgres',
  password: 'qwerty',
  port: 5432,
});

async function initDB() {
  try {
    await sequelize.authenticate();
    console.log("Connection has been established successfully.");
    // await sequelize.dropSchema("public",{});
    // await sequelize.createSchema("public",{});
    await sequelize.sync();
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
}

class Student extends Sequelize.Model {}

Student.init(
  {
    id: {
      type: Sequelize.DataTypes.UUID,
      defaultValue: Sequelize.DataTypes.UUIDV4,
      primaryKey: true,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    surname: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    patronymic: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Birthdate: {
      type: Sequelize.DATE,
      allowNull: false,
    },
  },
  { sequelize: sequelize, modelName: "student" }
);

class Group extends Sequelize.Model {}

Group.init(
  {
    id: {
      type: Sequelize.DataTypes.UUID,
      defaultValue: Sequelize.DataTypes.UUIDV4,
      primaryKey: true,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  },
  { sequelize: sequelize, modelName: "group" }
);

Group.hasMany(Student);

Student.belongsTo(Group, {
  foreignKey: "groupId",
});


async function bootstrap() {
    await initDB();
    Student.create({
        name: 'Иван',
        surname: 'Иванов',
        patronymic: '123ASD',
        groupid: '1',
        Birthdate: new Date('2001-09-18'),
    });
    Student.create({
        name: 'Сергей',
        surname: 'Сергеенко',
        patronymic: '125ASD',
        groupid: '2',
        Birthdate: new Date('2002-02-11'),
    });
    Group.create({
        name: 'БСБО-06'
    })
    
    Group.create({
        name: 'БСБО-07'
    })
    const studList = await Student.findAll({
      order:['Birthdate','ASC'],
    });

    writeFileSync('output.json', JSON.stringify(studentList));
};

bootstrap();
  
  